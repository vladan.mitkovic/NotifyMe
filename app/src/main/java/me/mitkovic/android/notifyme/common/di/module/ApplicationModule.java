package me.mitkovic.android.notifyme.common.di.module;

import android.app.Activity;
import android.app.Service;
import android.content.Context;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import me.mitkovic.android.notifyme.common.NotifyMeApplication;
import me.mitkovic.android.notifyme.common.di.scope.ApplicationContext;
import me.mitkovic.android.notifyme.common.di.scope.NotifyMeApplicatonScope;

@Module
public abstract class ApplicationModule {

    @NotifyMeApplicatonScope
    @Binds
    abstract AndroidInjector<Activity> bindsActivityInjector(DispatchingAndroidInjector<Activity> injector);

    @NotifyMeApplicatonScope
    @Binds
    abstract AndroidInjector<Service> bindsServiceInjector(DispatchingAndroidInjector<Service> injector);

    @Provides
    @NotifyMeApplicatonScope
    @ApplicationContext
    public static Context providesContext(NotifyMeApplication application) {
        return application.getApplicationContext();
    }

}
