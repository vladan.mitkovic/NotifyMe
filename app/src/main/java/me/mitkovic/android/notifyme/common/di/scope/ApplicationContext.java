package me.mitkovic.android.notifyme.common.di.scope;

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
