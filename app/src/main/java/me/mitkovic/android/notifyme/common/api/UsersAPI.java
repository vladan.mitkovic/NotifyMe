package me.mitkovic.android.notifyme.common.api;


import java.util.List;

import io.reactivex.Observable;
import me.mitkovic.android.notifyme.common.api.model.User;
import retrofit2.http.GET;

public interface UsersAPI {

    @GET("getuserslist")
    Observable<List<User>> getUsers();
}
