package me.mitkovic.android.notifyme.common.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import me.mitkovic.android.notifyme.BuildConfig;
import me.mitkovic.android.notifyme.common.api.UsersAPI;
import me.mitkovic.android.notifyme.common.di.scope.NotifyMeApplicatonScope;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class APIModule {

    @Provides
    @NotifyMeApplicatonScope
    public OkHttpClient providesOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        HttpLoggingInterceptor headerLogging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        HttpLoggingInterceptor bodyLogging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(headerLogging)
                .addInterceptor(bodyLogging)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @NotifyMeApplicatonScope
    public Gson providesGson(GsonBuilder gsonBuilder) {
        return gsonBuilder.create();
    }

    @Provides
    @NotifyMeApplicatonScope
    public GsonBuilder providesGsonBuilder() {
        return new GsonBuilder();
    }

    @Provides
    @NotifyMeApplicatonScope
    public Retrofit retrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .baseUrl(BuildConfig.API_ENDPOINT)
                .build();
    }

    @Provides
    @NotifyMeApplicatonScope
    public UsersAPI usersService(Retrofit retrofit) {
        return retrofit.create(UsersAPI.class);
    }

}
