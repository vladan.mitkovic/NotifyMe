package me.mitkovic.android.notifyme.home.view;


import android.content.Context;

import me.mitkovic.android.notifyme.common.api.model.User;
import me.mitkovic.android.notifyme.common.view.Presenter;

public interface MainPresenter extends Presenter<MainView> {

    void onClickUser(Context context, User user);

}
