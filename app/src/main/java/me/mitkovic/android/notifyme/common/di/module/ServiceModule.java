package me.mitkovic.android.notifyme.common.di.module;

import android.app.Service;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.android.ServiceKey;
import dagger.multibindings.IntoMap;
import me.mitkovic.android.notifyme.common.services.UsersService;
import me.mitkovic.android.notifyme.common.services.di.UsersServiceComponent;

@Module(subcomponents = {
        UsersServiceComponent.class
})
public abstract class ServiceModule {

    @Binds
    @IntoMap
    @ServiceKey(UsersService.class)
    abstract AndroidInjector.Factory<? extends Service>
        bindUsersServiceComponentInjectorFactory(UsersServiceComponent.Builder builder);

}
