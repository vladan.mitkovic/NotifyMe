package me.mitkovic.android.notifyme.common.services;

import java.util.List;

import io.reactivex.subjects.PublishSubject;
import me.mitkovic.android.notifyme.common.api.model.User;


public class AppEventDispatcher {

    public static final PublishSubject<List<User>> onEndServiceWork = PublishSubject.create();

}
