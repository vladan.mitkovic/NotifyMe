package me.mitkovic.android.notifyme.common.di;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import me.mitkovic.android.notifyme.common.NotifyMeApplication;
import me.mitkovic.android.notifyme.common.di.module.APIModule;
import me.mitkovic.android.notifyme.common.di.module.AppModule;
import me.mitkovic.android.notifyme.common.di.module.ApplicationModule;
import me.mitkovic.android.notifyme.common.di.module.ServiceModule;
import me.mitkovic.android.notifyme.common.di.scope.NotifyMeApplicatonScope;


@NotifyMeApplicatonScope
@Component(modules = {
        AndroidInjectionModule.class,
        ApplicationModule.class,
        APIModule.class,
        AppModule.class,
        ServiceModule.class
})
public interface ApplicationComponent {

    void inject(NotifyMeApplication notifyMeApplication);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(NotifyMeApplication application);
        ApplicationComponent build();
    }

}
