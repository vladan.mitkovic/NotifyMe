package me.mitkovic.android.notifyme.common.services.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import me.mitkovic.android.notifyme.common.services.UsersService;

@Subcomponent
public interface UsersServiceComponent extends AndroidInjector<UsersService> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<UsersService> {}

}
