package me.mitkovic.android.notifyme.common.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import me.mitkovic.android.notifyme.common.di.scope.PerActivity;
import me.mitkovic.android.notifyme.home.di.MainModule;
import me.mitkovic.android.notifyme.home.view.MainActivity;

@Module
public abstract class AppModule {

    @PerActivity
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity reposActivityInjector();
}
