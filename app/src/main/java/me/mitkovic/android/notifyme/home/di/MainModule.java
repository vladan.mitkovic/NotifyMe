package me.mitkovic.android.notifyme.home.di;

import dagger.Binds;
import dagger.Module;
import me.mitkovic.android.notifyme.common.di.scope.PerActivity;
import me.mitkovic.android.notifyme.home.view.MainPresenter;
import me.mitkovic.android.notifyme.home.view.MainPresenterImpl;

@PerActivity
@Module
public abstract class MainModule {

    @Binds
    abstract MainPresenter bindsMainPresenter(MainPresenterImpl presenter);

}
