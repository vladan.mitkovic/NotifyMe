package me.mitkovic.android.notifyme.common;

import android.app.Activity;
import android.app.Application;
import android.app.Service;

import com.facebook.drawee.backends.pipeline.Fresco;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;
import me.mitkovic.android.notifyme.common.di.DaggerApplicationComponent;

public class NotifyMeApplication extends Application implements HasActivityInjector, HasServiceInjector {

    @Inject
    AndroidInjector<Activity> activityInjector;

    @Inject
    AndroidInjector<Service> serviceInjector;

    @Override
    public void onCreate() {
        super.onCreate();

        setup();
    }

    protected void setup() {
        DaggerApplicationComponent
                .builder()
                .application(this)
                .build()
                .inject(this);

        Fresco.initialize(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return serviceInjector;
    }

}
