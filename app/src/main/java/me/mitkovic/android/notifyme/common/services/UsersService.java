package me.mitkovic.android.notifyme.common.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import hugo.weaving.DebugLog;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import me.mitkovic.android.notifyme.common.api.UsersAPI;
import me.mitkovic.android.notifyme.common.api.model.User;

public class UsersService extends Service {

    private Looper looper;
    private ServiceHandler serviceHandler;

    @Inject
    UsersAPI usersAPI;

    private CompositeDisposable subscription;

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidInjection.inject(this);

        this.subscription = new CompositeDisposable();

        HandlerThread handlerThread = new HandlerThread("WorkerThread",
                Process.THREAD_PRIORITY_BACKGROUND);
        handlerThread.start();

        looper = handlerThread.getLooper();
        serviceHandler = new ServiceHandler(looper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Message message = serviceHandler.obtainMessage();
        message.arg1 = startId;
        serviceHandler.sendMessage(message);

        return START_STICKY;
    }

    private final class ServiceHandler extends Handler {

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {

            Disposable serviceSubscription = usersAPI.getUsers()
                .subscribe(successConsumer(), errorConsumer());

            subscription.add(serviceSubscription);

            stopSelf(msg.arg1);
        }

        private Consumer<List<User>> successConsumer() {
            return new Consumer<List<User>>() {

                @Override
                @DebugLog
                public void accept(final List<User> users) throws Exception {
                    AppEventDispatcher.onEndServiceWork.onNext(users);
                    AppEventDispatcher.onEndServiceWork.onComplete();
                }
            };
        }

        private Consumer<Throwable> errorConsumer() {
            return new Consumer<Throwable>() {
                @Override
                public void accept(Throwable throwable) throws Exception {

                }
            };
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (subscription != null) {
            subscription.dispose();
            subscription = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
