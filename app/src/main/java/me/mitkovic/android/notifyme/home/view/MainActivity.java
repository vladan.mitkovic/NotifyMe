package me.mitkovic.android.notifyme.home.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import me.mitkovic.android.notifyme.R;
import me.mitkovic.android.notifyme.common.api.model.User;
import me.mitkovic.android.notifyme.common.services.UsersService;

public class MainActivity extends AppCompatActivity implements MainView, UserRow.Listener {

    @Inject
    MainPresenter presenter;

    @Inject
    UsersListAdapter usersAdapter;

    @BindView(R.id.repo_list)
    RecyclerView recyclerView;

    @BindView(R.id.loading)
    View loading;

    @BindView(R.id.layout_holder)
    RelativeLayout layoutHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        usersAdapter.setListener(this);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(usersAdapter);

        Intent usersService = new Intent(this, UsersService.class);
        startService(usersService);

        if (savedInstanceState != null) {
            presenter.onRestore(savedInstanceState);
        }
        presenter.onBind(this);
    }

    @Override
    public void onClickUser(User user) {
        presenter.onClickUser(MainActivity.this, user);
    }

    @Override
    public void setAdapterData(List<User> users) {
        usersAdapter.setUsers(users);
    }

    @Override
    public void setSuccessSnackBar() {
        String successText = getString(R.string.success_message);
        Snackbar snackbar = Snackbar.make(layoutHolder, successText, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void setErrorMessage(String errorMessage) {
        Toast.makeText(MainActivity.this, getString(R.string.error_users_message) + errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setLoading(boolean isLoading) {
        loading.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (presenter != null)
            presenter.onSave(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null)
            presenter.onUnbind();
    }

}
