package me.mitkovic.android.notifyme.home.view;


import java.util.List;

import me.mitkovic.android.notifyme.common.api.model.User;
import me.mitkovic.android.notifyme.common.view.View;

public interface MainView extends View {

    void setAdapterData(List<User> users);

    void setSuccessSnackBar();

    void setErrorMessage(String errorMessage);

    void setLoading(boolean isLoading);

}
